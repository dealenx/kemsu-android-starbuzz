package kemsu.dealenx.starbuzz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import kemsu.dealenx.starbuzz.store.StoreActivity;

public class StartActivity extends AppCompatActivity {

    MyApplication myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getSupportActionBar().hide();
        myApp = (MyApplication)getApplicationContext();
    }

    public void clickNextPage(View view) {
        Intent nextScreen = new Intent(this, StoreActivity.class);

        startActivity(nextScreen);
    }

    /*public void startService(View view) {
        myApp.startService();
    }*/


}
