package kemsu.dealenx.starbuzz.info;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import kemsu.dealenx.starbuzz.MyApplication;
import kemsu.dealenx.starbuzz.R;
import kemsu.dealenx.starbuzz.cart.CartActivity;
import kemsu.dealenx.starbuzz.menu.MenuActivity;
import kemsu.dealenx.starbuzz.menu.MenuEntity;
import kemsu.dealenx.starbuzz.store.StoreEntity;

public class InfoActivity extends AppCompatActivity {

    MyApplication myApp;

    StoreEntity storeActive;

    MenuEntity menuEntityActive;

    TextView textTitle;
    TextView textPrice;
    TextView textDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        myApp = (MyApplication)getApplicationContext();
        storeActive =  myApp.storeModel.getActiveStore();
        menuEntityActive = storeActive.menuModel.allMenu.get(storeActive.activeProductID);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCartCounter();
                clickMenu();
            }
        });
        setToolbar("Описание");

        textTitle = findViewById(R.id.textTitle);
        textTitle.setText(menuEntityActive.getName());

        textPrice = findViewById(R.id.textPrice);
        textPrice.setText(String.valueOf(menuEntityActive.getPrice()) + " \u20BD");

        textDesc = findViewById(R.id.textDesc);
        textDesc.setText(String.valueOf(menuEntityActive.getDesc()));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_nav, menu);
        MenuItem item = menu.findItem(R.id.action_cart);
        item.setTitle("Корзина(" + storeActive.cartModel.getCounter() + ")");
        return true;
    }

    private void setToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (toolbar != null) {
            toolbar.setTitle(title);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        }

        toolbar.inflateMenu(R.menu.menu_activity_nav);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_cart:
                        Toast.makeText(getApplicationContext(),
                                "New order", Toast.LENGTH_SHORT).show();
                        clickCart();
                        return true;
                }
                //If above criteria does not meet then default is false;
                return false;
            }
        });
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public static void refreshMenu(Activity activity)
    {
        activity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_cart:
                Toast.makeText(getApplicationContext(),"Settings Option Selected",Toast.LENGTH_SHORT).show();
                clickCart();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addCartCounter() {
        //storeActive.cartModel.plusCounter();

        storeActive.cartModel.addProduct(storeActive.menuModel.allMenu.get(storeActive.activeProductID));

        Toast.makeText(getApplicationContext(),"cart counter is " + storeActive.cartModel.getCounter(),Toast.LENGTH_SHORT).show();
        refreshMenu(this);
    }

    public void clickCart() {
        Intent nextScreen = new Intent(this, CartActivity.class);

        startActivity(nextScreen);
    }

    public void clickMenu() {
        Intent nextScreen = new Intent(this, MenuActivity.class);

        startActivity(nextScreen);
    }

}
