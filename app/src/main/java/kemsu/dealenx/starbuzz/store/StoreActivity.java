package kemsu.dealenx.starbuzz.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.MapsActivity;
import kemsu.dealenx.starbuzz.cart.CartModel;
import kemsu.dealenx.starbuzz.menu.MenuActivity;
import kemsu.dealenx.starbuzz.MyApplication;
import kemsu.dealenx.starbuzz.R;

public class StoreActivity extends AppCompatActivity {

    MyApplication myApp;

    String TAG = "StoresActivity";

    View viewRoot;

    ArrayList<StoreEntity> storesData;

    CartModel cartModel = new CartModel();


    public void initListView() {
        /*listView*/
        storesData = myApp.storeModel.getStoresData();

        StoreListAdapter storeListAdapter = new StoreListAdapter(storesData, this);
        ListView storeListView = (ListView) findViewById(R.id.listVIewID);
        storeListView.setAdapter(storeListAdapter);

        storeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {

                Toast.makeText(getApplicationContext(),
                        "You click on position:"+position, Toast.LENGTH_SHORT).show();
                myApp.storeModel.setActiveStoreID(position);
                clickNextPage(viewRoot);
            }
        });
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_nav, menu);
        return true;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        setToolbar("Кофейни");

        myApp = (MyApplication)getApplicationContext();

        viewRoot = getWindow().getDecorView().getRootView();

        initListView();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                clickMapsPage();
            }
        });
    }

    private void setToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
            if (toolbar != null) {
            toolbar.setTitle(title);
        }
        setSupportActionBar(toolbar);
    }

    public void clickNextPage(View view) {
        Intent nextScreen = new Intent(this, MenuActivity.class);

        startActivity(nextScreen);
    }

    public void clickMapsPage() {
        Intent nextScreen = new Intent(this, MapsActivity.class);

        startActivity(nextScreen);
    }

    /*public void stopService (View view) {
        myApp.stopService();
    }*/

}
