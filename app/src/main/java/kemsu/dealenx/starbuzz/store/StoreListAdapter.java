package kemsu.dealenx.starbuzz.store;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.R;
import kemsu.dealenx.starbuzz.store.StoreEntity;

public class StoreListAdapter extends ArrayAdapter<StoreEntity> {

    //to reference the Activity
    private Activity context;
    private Context mContext;

    //to store the list of countries
    private String[] nameArray;

    //to store the list of countries
    private String[] infoArray;

    private ArrayList<StoreEntity> stores;

    public StoreListAdapter(ArrayList<StoreEntity> stores, Activity context) {
        super(context, R.layout.store_listview_row, stores);
        this.stores = stores;
        this.context=context;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.store_listview_row, null,true);

        //this code gets references to objects in the store_listview_row.xmlow.xml file
        TextView nameTextField = (TextView) rowView.findViewById(R.id.nameTextViewID);
        TextView infoTextField = (TextView) rowView.findViewById(R.id.infoTextViewID);

        //this code sets the values of the objects to values from the arrays
        nameTextField.setText(stores.get(position).getName());
        infoTextField.setText(stores.get(position).getAddress());


        return rowView;
    };
}
