package kemsu.dealenx.starbuzz.store;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.cart.CartModel;
import kemsu.dealenx.starbuzz.menu.MenuEntity;
import kemsu.dealenx.starbuzz.menu.MenuModel;

public class StoreEntity {
    private String name;
    private String address;

    public MenuModel menuModel;
    public CartModel cartModel;

    public int activeProductID;

    public StoreEntity(String name, String address) {
        menuModel = new MenuModel();
        cartModel = new CartModel();
        this.name = name;
        this.address = address;
    }

    public void initMenu(MenuModel menuModel) {
        this.menuModel = null;
        this.menuModel = menuModel;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void resetCart() {
        cartModel = new CartModel();
    }
}
