package kemsu.dealenx.starbuzz.store;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.menu.MenuEntity;
import kemsu.dealenx.starbuzz.menu.MenuModel;

public class StoreModel {

    ArrayList<StoreEntity> storesData;

    public int activeStoreID;

    public StoreModel() {
        storesData = new ArrayList<>();
        storesData.add(new StoreEntity("Главная Штаб-Кофейня!", "Советский проспект, 32"));
        storesData.add(new StoreEntity("Ламповая тихая кофейня", "Советский проспект, 48A"));

        init();
    }

    public void init() {

        for(int i =0; i < storesData.size(); i++) {
            loadInfromation(i);
        }

    }

    public void loadInfromation(int indexStore) {
        ArrayList<MenuEntity> allMenu = new ArrayList<>();
        allMenu.add(new MenuEntity(0,"Кофе #1", 139, "Напитки", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "));
        allMenu.add(new MenuEntity(1,"Кофе #2", 167, "Напитки", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "));
        allMenu.add(new MenuEntity(2,"Шаверма #1", 145, "Блюда", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "));
        allMenu.add(new MenuEntity(3,"Шаверма #2", 175, "Блюда", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "));

        updateMenuToID(indexStore, allMenu);
    }

    public void updateMenuToID(int id, ArrayList<MenuEntity> allMenu ) {
        storesData.get(id).menuModel.setAllMenu(allMenu);

    }

    public void setActiveStoreID(int activeStoreID) {
        this.activeStoreID = activeStoreID;
    }

    public int getActiveStoreID() {
        return activeStoreID;
    }

    public StoreEntity getActiveStore() {
        return storesData.get(getActiveStoreID());
    }

    public MenuModel getActiveMenuModel() {
        return storesData.get(getActiveStoreID()).menuModel;
    }

    public ArrayList<StoreEntity> getStoresData() {
        return storesData;
    }
}
