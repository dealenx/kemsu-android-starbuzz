package kemsu.dealenx.starbuzz;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.View;

import kemsu.dealenx.starbuzz.cart.CartModel;
import kemsu.dealenx.starbuzz.menu.MenuModel;
import kemsu.dealenx.starbuzz.store.StoreModel;

public class MyApplication extends Application {

    private static MyApplication singleton;

    public StoreModel storeModel;
    public CartModel cartModel;

    public static MyApplication getInstance(){
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        storeModel= new StoreModel();
        cartModel = new CartModel();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    /*public void startService() {
        startService(new Intent(this, MyService.class));
    }

    // Stop the service
    public void stopService() {
        stopService(new Intent(this, MyService.class));
    }*/


}