package kemsu.dealenx.starbuzz.cart;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.menu.MenuEntity;

public class CartModel {
    int counter = 0;

    double sumPrices = 0;

    public ArrayList<MenuEntity> selectedProducts;

    public CartModel() {
        initProducts();
    }

    public void initProducts() {
        selectedProducts = new ArrayList<>();
        //selectedProducts.add(new MenuEntity(0, "Кофе #1", 139, "Напитки"));

        update();
    }

    void update() {
        sumPrices = 0;
        for(int i =0; i < selectedProducts.size(); i++) {
            sumPrices += selectedProducts.get(i).getPrice();
        }

        counter = selectedProducts.size();
    }

    public double getSumPrices() {
        return sumPrices;
    }

    public void addProduct(MenuEntity newProduct) {
        selectedProducts.add(newProduct);

        update();
    }

    public int getCounter() {
        return counter;
    }

    public void plusCounter() {
        this.counter++;
    }
}
