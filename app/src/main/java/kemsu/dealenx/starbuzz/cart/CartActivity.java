package kemsu.dealenx.starbuzz.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.MyApplication;
import kemsu.dealenx.starbuzz.R;
import kemsu.dealenx.starbuzz.info.InfoActivity;
import kemsu.dealenx.starbuzz.menu.MenuEntity;
import kemsu.dealenx.starbuzz.menu.MenuListAdapter;
import kemsu.dealenx.starbuzz.store.StoreActivity;
import kemsu.dealenx.starbuzz.store.StoreEntity;

public class CartActivity extends AppCompatActivity {


    MyApplication myApp;
    StoreEntity storeActive;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = (MyApplication)getApplicationContext();
        storeActive =  myApp.storeModel.getActiveStore();

        setContentView(R.layout.activity_cart);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickStore();
                storeActive.resetCart();
                Toast.makeText(getApplicationContext(),"Заказ оплачен и отправлен",Toast.LENGTH_SHORT).show();
            }
        });

        initListView(storeActive.cartModel.selectedProducts);

        setToolbar("Корзина");
    }

    private void setToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (toolbar != null) {
            toolbar.setTitle(title);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        }

        toolbar.inflateMenu(R.menu.menu_activity_nav);

        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_nav, menu);
        MenuItem item = menu.findItem(R.id.action_cart);
        item.setTitle(storeActive.cartModel.getSumPrices() + " \u20BD");
        return true;
    }

    public void initListView(ArrayList<MenuEntity> menu) {
        /*listView*/

        CartListAdapter cartListAdapter = new CartListAdapter(menu, this);
        ListView menuListView = (ListView) findViewById(R.id.listVIewID);
        menuListView.setAdapter(cartListAdapter);

        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {

            }
        });
    }

    public void clickStore() {
        Intent nextScreen = new Intent(this, StoreActivity.class);

        startActivity(nextScreen);
    }

}
