package kemsu.dealenx.starbuzz.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.cart.CartActivity;
import kemsu.dealenx.starbuzz.info.InfoActivity;
import kemsu.dealenx.starbuzz.MyApplication;
import kemsu.dealenx.starbuzz.R;
import kemsu.dealenx.starbuzz.store.StoreEntity;

public class MenuActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private LinearLayout drinksLayout;
    private LinearLayout dishesLayout;

    MyApplication myApp;
    StoreEntity storeActive;

    ArrayList<MenuEntity> menuData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = (MyApplication)getApplicationContext();
        storeActive =  myApp.storeModel.getActiveStore();

        setContentView(R.layout.activity_menu);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        initListView(myApp.storeModel.getActiveMenuModel().getMenuDrinks());

        Toast.makeText(getApplicationContext(),
                "active store id :"+myApp.storeModel.getActiveStoreID(), Toast.LENGTH_SHORT).show();


        setToolbar("Меню");
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_drinks:
                    initListView(storeActive.menuModel.getMenuDrinks());
                    return true;
                case R.id.navigation_dishes:
                    initListView(storeActive.menuModel.getMenuDishes());
                    return true;

            }
            return false;
        }
    };





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_nav, menu);
        MenuItem item = menu.findItem(R.id.action_cart);
        item.setTitle("Корзина(" + storeActive.cartModel.getCounter() + ")");
        return true;
    }

    public void initListView(ArrayList<MenuEntity> menu) {
        /*listView*/

        MenuListAdapter menuListAdapter = new MenuListAdapter(menu, this);
        menuData = menu;
        ListView menuListView = (ListView) findViewById(R.id.listVIewID);
        menuListView.setAdapter(menuListAdapter);

        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {

                storeActive.activeProductID = menuData.get(position).getId();

                Toast.makeText(getApplicationContext(),
                        "active product:"+storeActive.activeProductID, Toast.LENGTH_SHORT).show();



                clickInfo();
                //clickNextPage(viewRoot);
            }
        });
    }

    private void setToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        //getSupportActionBar().hide();

        if (toolbar != null) {
            //setSupportActionBar(toolbar);
            toolbar.setTitle(title);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        }

        toolbar.inflateMenu(R.menu.menu_activity_nav);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    //Change the ImageView image source depends on menu item click
                    case R.id.action_cart:
                        Toast.makeText(getApplicationContext(),
                                "New order", Toast.LENGTH_SHORT).show();
                        clickCart();
                        return true;
                }
                //If above criteria does not meet then default is false;
                return false;
            }
        });

        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_cart:
                Toast.makeText(getApplicationContext(),"Settings Option Selected",Toast.LENGTH_SHORT).show();
                clickCart();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void clickCart() {
        Intent nextScreen = new Intent(this, CartActivity.class);

        startActivity(nextScreen);
    }

    public void clickInfo() {
        Intent nextScreen = new Intent(this, InfoActivity.class);

        startActivity(nextScreen);
    }


}
