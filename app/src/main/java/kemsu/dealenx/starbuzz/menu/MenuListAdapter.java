package kemsu.dealenx.starbuzz.menu;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kemsu.dealenx.starbuzz.R;

public class MenuListAdapter extends ArrayAdapter<MenuEntity> {

    //to reference the Activity
    private Activity context;
    private Context mContext;

    //to store the list of countries
    private String[] nameArray;

    //to store the list of countries
    private String[] infoArray;

    private ArrayList<MenuEntity> menu;

    public MenuListAdapter(ArrayList<MenuEntity> menu, Activity context) {
        super(context, R.layout.menu_listview_row, menu);
        this.menu = menu;
        this.context=context;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.menu_listview_row, null,true);

        //this code gets references to objects in the store_listview_row.xmlow.xml file
        TextView nameTextField = (TextView) rowView.findViewById(R.id.nameTextViewID);
        TextView infoTextField = (TextView) rowView.findViewById(R.id.infoTextViewID);

        //this code sets the values of the objects to values from the arrays
        nameTextField.setText(menu.get(position).getName());
        infoTextField.setText(String.valueOf(menu.get(position).getPrice()) + " \u20BD");


        return rowView;
    };
}
