package kemsu.dealenx.starbuzz.menu;

public class MenuEntity {
    private String name;
    private double price;
    private int id;

    private String category;
    private String desc;

    public MenuEntity(int id, String name, double price, String category, String desc) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
