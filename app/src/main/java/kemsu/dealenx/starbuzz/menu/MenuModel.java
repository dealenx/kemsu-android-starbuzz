package kemsu.dealenx.starbuzz.menu;

import java.util.ArrayList;

public class MenuModel {

    public ArrayList<MenuEntity> allMenu;
    public ArrayList<MenuEntity> menuDrinks;
    public ArrayList<MenuEntity> menuDishes;

    public MenuModel() {
        menuDrinks = new ArrayList<>();
        menuDishes = new ArrayList<>();

    }

    public ArrayList<MenuEntity> getAllMenu() {
        return allMenu;
    }

    public void setAllMenu(ArrayList<MenuEntity> allMenu) {
        this.allMenu = allMenu;


        for (int i = 0; i < allMenu.size(); i++) {
            if (allMenu.get(i).getCategory().equals("Напитки")) {
                menuDrinks.add(allMenu.get(i));
            }
        }

        for (int i = 0; i < allMenu.size(); i++) {
            if (allMenu.get(i).getCategory().equals("Блюда")) {
                menuDishes.add(allMenu.get(i));
            }
        }


    }

    public ArrayList<MenuEntity> getMenuDrinks() {
        return menuDrinks;
    }

    public ArrayList<MenuEntity> getMenuDishes() {
        return menuDishes;
    }

    public void setMenuDrinks(ArrayList<MenuEntity> menuDrinks) {
        this.menuDrinks = menuDrinks;
    }

    public void setMenuDishes(ArrayList<MenuEntity> menuDishes) {
        this.menuDishes = menuDishes;
    }
}
